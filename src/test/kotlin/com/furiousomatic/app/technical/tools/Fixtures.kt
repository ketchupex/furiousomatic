package com.furiousomatic.app.technical.tools

import java.time.Instant
import kotlin.random.Random

fun randomInstant(): Instant = Instant.ofEpochSecond(Random.nextLong(1590000000, 1700000000))
fun randomInt(max: Int = 9999) = Random.nextInt(0, max)
private val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
fun randomString(length: Int = 10, prefix: String = "") =
    prefix + (1..length)
        .map { Random.nextInt(0, charPool.size) }
        .map(charPool::get)
        .joinToString("")
