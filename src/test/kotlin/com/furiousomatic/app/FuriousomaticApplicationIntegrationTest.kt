package com.furiousomatic.app

import com.furiousomatic.app.technical.tools.DatabaseContainerInitializer
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.ConfigDataApplicationContextInitializer
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration

@ActiveProfiles("local")
@SpringBootTest
@ContextConfiguration(
    initializers = [DatabaseContainerInitializer::class, ConfigDataApplicationContextInitializer::class]
)
class FuriousomaticApplicationIntegrationTest {

    @Test
    fun contextLoads() {
    }
}
