package com.furiousomatic.app.showmanagement.infrastructure

import io.mockk.mockk
import org.junit.Ignore
import org.junit.jupiter.api.Test
import java.time.Clock

class ScreeningRoomDbRepositoryTest {
    private val screeningRoomEntityRepository: ScreeningRoomEntityRepository = mockk()
    private val clock: Clock = mockk()
    private val screeningRoomDbRepository = ScreeningRoomDbRepository(screeningRoomEntityRepository, clock)

    @Ignore
    @Test
    fun `should get screening room`() {
        // TODO
    }

    @Ignore
    @Test
    fun `should throw exception for not existing screening room`() {
        // TODO
    }

    @Ignore
    @Test
    fun `should save screening room`() {
        // TODO
    }
}
