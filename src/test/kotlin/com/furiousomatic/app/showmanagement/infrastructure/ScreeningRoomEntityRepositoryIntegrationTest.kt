package com.furiousomatic.app.showmanagement.infrastructure

import com.furiousomatic.app.showmanagement.tools.TransactioWrapperConfig
import com.furiousomatic.app.showmanagement.tools.TransactionWrapper
import com.furiousomatic.app.technical.tools.DatabaseContainerInitializer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.Import
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import strikt.api.expectThat
import strikt.assertions.containsExactlyInAnyOrder
import strikt.assertions.isEqualTo
import java.math.BigDecimal
import java.time.Instant
import java.util.UUID

@DataJpaTest(showSql = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(
    initializers = [DatabaseContainerInitializer::class]
)
@Import(TransactioWrapperConfig::class)
class ScreeningRoomEntityRepositoryIntegrationTest {
    @Autowired
    private lateinit var showTimeRepository: ShowTimeEntityRepository

    @Autowired
    private lateinit var screeningRoomRepository: ScreeningRoomEntityRepository

    @Autowired
    private lateinit var transactionWrapper: TransactionWrapper

    @AfterEach
    fun cleanup() {
        showTimeRepository.deleteAll()
        screeningRoomRepository.deleteAll()
    }

    @Test
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun `should save and get screening room`() {
        // given
        val screeningRoomId = UUID.randomUUID()
        val showTime1 = ShowTimeEntity(
            id = UUID.randomUUID(),
            screeningRoomId = screeningRoomId,
            movieId = UUID.randomUUID(),
            start = Instant.now(),
            end = Instant.now().plusSeconds(1234),
            price = BigDecimal.valueOf(123),
            currency = "USD"
        )
        val showTime2 = ShowTimeEntity(
            id = UUID.randomUUID(),
            screeningRoomId = screeningRoomId,
            movieId = UUID.randomUUID(),
            start = Instant.now(),
            end = Instant.now().plusSeconds(1234),
            price = BigDecimal.valueOf(123),
            currency = "USD"
        )
        val screeningRoom = ScreeningRoomEntity(screeningRoomId, listOf(showTime1, showTime2))
        // when
        transactionWrapper {
            screeningRoomRepository.save(screeningRoom)
        }
        val result = screeningRoomRepository.findById(screeningRoom.id).get()
        // then
        expectThat(result.id).isEqualTo(screeningRoom.id)
        expectThat(result.showTimes).containsExactlyInAnyOrder(showTime1, showTime2)
    }
}
