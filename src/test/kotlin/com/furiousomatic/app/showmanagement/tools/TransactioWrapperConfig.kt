package com.furiousomatic.app.showmanagement.tools

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.TransactionStatus
import org.springframework.transaction.support.TransactionTemplate

@Configuration
class TransactioWrapperConfig {

    @Bean
    fun jpaTransactionWrapper(transactionManager: PlatformTransactionManager): TransactionWrapper {
        return JpaTransactionWrapper(TransactionTemplate(transactionManager))
    }
}

interface TransactionWrapper {
    operator fun <T> invoke(transactionalScope: (status: TransactionStatus) -> T): T
}

private class JpaTransactionWrapper(private val transactionTemplate: TransactionTemplate) : TransactionWrapper {

    override fun <T> invoke(transactionalScope: (status: TransactionStatus) -> T): T {
        return transactionTemplate.execute { status -> transactionalScope(status) }!!
    }
}
