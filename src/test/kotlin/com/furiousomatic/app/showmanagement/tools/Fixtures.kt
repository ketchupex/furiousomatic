package com.furiousomatic.app.showmanagement.tools

import com.furiousomatic.app.showmanagement.deepmodel.Movie
import com.furiousomatic.app.showmanagement.deepmodel.ScreeningRoom
import com.furiousomatic.app.showmanagement.deepmodel.ShowTime
import com.furiousomatic.app.technical.tools.randomInstant
import com.furiousomatic.app.technical.tools.randomInt
import org.javamoney.moneta.Money
import java.time.Clock
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID
import javax.money.Monetary
import javax.money.MonetaryAmount

internal fun randomScreeningRoom(clock: Clock = Clock.systemDefaultZone(), showTimes: List<ShowTime>) =
    ScreeningRoom(UUID.randomUUID(), clock, showTimes)

internal fun randomShowTime(
    movie: Movie = randomMovie(),
    start: Instant = randomInstant(),
    end: Instant = start.plus(2, ChronoUnit.HOURS),
    price: MonetaryAmount = Money.of(randomInt(10), Monetary.getCurrency("USD")),
    clock: Clock = Clock.systemDefaultZone()
) = ShowTime(UUID.randomUUID(), UUID.randomUUID(), movie, start, end, price, clock)

internal fun randomMovie() = Movie(UUID.randomUUID())
