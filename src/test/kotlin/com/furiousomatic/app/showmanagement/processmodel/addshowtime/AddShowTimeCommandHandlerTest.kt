package com.furiousomatic.app.showmanagement.processmodel.addshowtime

import com.furiousomatic.app.showmanagement.deepmodel.ActiveMovieRequiredException
import com.furiousomatic.app.showmanagement.deepmodel.Movie
import com.furiousomatic.app.showmanagement.deepmodel.MovieActivityChecker
import com.furiousomatic.app.showmanagement.deepmodel.ScreeningRoom
import com.furiousomatic.app.showmanagement.deepmodel.ScreeningRoomRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import strikt.api.expectThrows
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID
import javax.money.Monetary

class AddShowTimeCommandHandlerTest {
    private val movieActivityChecker: MovieActivityChecker = mockk()
    private val screeningRoomRepository: ScreeningRoomRepository = mockk()
    private val handler = AddShowTimeCommandHandler(movieActivityChecker, screeningRoomRepository)

    @Test
    fun `should add show time`() {
        // given
        val movieId = UUID.randomUUID()
        every { movieActivityChecker.isActive(movieId) } returns true
        val screeningRoom: ScreeningRoom = mockk()
        val screeningRoomId = UUID.randomUUID()
        val start = Instant.now()
        val end = start.plus(2, ChronoUnit.HOURS)
        val price = Money.of(10, Monetary.getCurrency("USD"))
        every { screeningRoomRepository.findById(screeningRoomId) } returns screeningRoom
        every { screeningRoom.addShowTime(Movie(movieId), start, end, price) } answers {}
        every { screeningRoomRepository.save(screeningRoom) } answers {}
        val command = AddShowTimeCommand(
            movieId = movieId,
            screeningRoomId = screeningRoomId,
            start = start,
            end = end,
            price = price
        )
        // when
        handler.handle(command)
        // then
        verify(exactly = 1) { movieActivityChecker.isActive(any()) }
        verify(exactly = 1) { screeningRoomRepository.findById(any()) }
        verify(exactly = 1) { screeningRoom.addShowTime(any(), any(), any(), command.price) }
        verify(exactly = 1) { screeningRoomRepository.save(any()) }
    }

    @Test
    fun `should not add show time because movie is not active`() {
        // given
        val movieId = UUID.randomUUID()
        every { movieActivityChecker.isActive(movieId) } returns false
        val screeningRoom: ScreeningRoom = mockk()
        val screeningRoomId = UUID.randomUUID()
        val start = Instant.now()
        val end = start.plus(2, ChronoUnit.HOURS)
        val price = Money.of(10, Monetary.getCurrency("USD"))
        val command = AddShowTimeCommand(
            movieId = movieId,
            screeningRoomId = screeningRoomId,
            start = start,
            end = end,
            price = price
        )
        // when
        expectThrows<ActiveMovieRequiredException> { handler.handle(command) }
        // then
        verify(exactly = 1) { movieActivityChecker.isActive(any()) }
        verify(exactly = 0) { screeningRoomRepository.findById(any()) }
        verify(exactly = 0) { screeningRoom.addShowTime(any(), any(), any(), command.price) }
        verify(exactly = 0) { screeningRoomRepository.save(any()) }
    }
}
