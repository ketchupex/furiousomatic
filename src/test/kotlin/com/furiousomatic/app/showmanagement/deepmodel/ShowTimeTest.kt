package com.furiousomatic.app.showmanagement.deepmodel

import com.furiousomatic.app.showmanagement.tools.randomShowTime
import io.mockk.every
import io.mockk.mockk
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.isEqualTo
import strikt.assertions.isFalse
import strikt.assertions.isTrue
import java.time.Clock
import java.time.Instant
import java.time.temporal.ChronoUnit
import javax.money.Monetary

class ShowTimeTest {

    @Test
    fun `should not overlap for one after another`() {
        // given
        val start = Instant.now()
        val end = start.plus(1, ChronoUnit.HOURS)
        val showTime = randomShowTime(start = start, end = end)
        val newShowTime = randomShowTime(start = end, end = end.plus(1, ChronoUnit.HOURS))
        // then
        expectThat(showTime.overlaps(newShowTime)).isFalse()
        expectThat(newShowTime.overlaps(showTime)).isFalse()
    }

    @Test
    fun `should overlap`() {
        // given
        val start = Instant.now()
        val end = start.plus(1, ChronoUnit.HOURS)
        val showTime = randomShowTime(start = start, end = end)
        val newShowTime = randomShowTime(start = end.minus(30, ChronoUnit.MINUTES), end = end.plus(1, ChronoUnit.HOURS))
        // then
        expectThat(showTime.overlaps(newShowTime)).isTrue()
        expectThat(newShowTime.overlaps(showTime)).isTrue()
    }

    @Test
    fun `should change price`() {
        // given
        val clock: Clock = mockk()
        val now = Instant.now()
        val oldPrice = Money.of(10, Monetary.getCurrency("USD"))
        val showTime = randomShowTime(
            start = now.plus(24, ChronoUnit.HOURS),
            end = now.plus(25, ChronoUnit.HOURS),
            price = oldPrice,
            clock = clock
        )
        val newPrice = Money.of(15, Monetary.getCurrency("USD"))
        every { clock.instant() } returns now
        // when
        showTime.changePrice(newPrice)
        // then
        expectThat(showTime.toSnapshot().price).isEqualTo(newPrice)
    }

    @Test
    fun `should not change price because it is too late`() {
        // given
        val clock: Clock = mockk()
        val now = Instant.now()
        val oldPrice = Money.of(10, Monetary.getCurrency("USD"))
        val showTime = randomShowTime(
            start = now.plus(24, ChronoUnit.HOURS).minusSeconds(1),
            end = now.plus(25, ChronoUnit.HOURS).minusSeconds(1),
            price = oldPrice,
            clock = clock
        )
        val newPrice = Money.of(15, Monetary.getCurrency("USD"))
        every { clock.instant() } returns now
        // when
        expectThrows<TooLateOperationException> { showTime.changePrice(newPrice) }
        // then
        expectThat(showTime.toSnapshot().price).isEqualTo(oldPrice)
    }
}
