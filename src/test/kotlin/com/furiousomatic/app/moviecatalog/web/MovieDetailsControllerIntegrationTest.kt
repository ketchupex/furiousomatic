package com.furiousomatic.app.moviecatalog.web

import com.furiousomatic.app.moviecatalog.deepmodel.MovieNotFoundException
import com.furiousomatic.app.moviecatalog.querymodel.moviedetails.MovieDetails
import com.furiousomatic.app.moviecatalog.querymodel.moviedetails.MovieDetailsFetcher
import com.furiousomatic.app.moviecatalog.querymodel.moviedetails.MovieDetailsNotAvailableException
import com.furiousomatic.app.technical.JacksonConfig
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import org.hamcrest.CoreMatchers.`is`
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.FilterType
import org.springframework.context.annotation.Import
import org.springframework.security.config.annotation.web.WebSecurityConfigurer
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.web.filter.OncePerRequestFilter
import java.util.UUID

@ExtendWith(SpringExtension::class)
@Import(JacksonConfig::class, MovieCatalogExceptionHandler::class)
@WebMvcTest(
    MovieDetailsController::class,
    excludeFilters = [
        ComponentScan.Filter(
            type = FilterType.ASSIGNABLE_TYPE,
            value = [WebSecurityConfigurer::class, OncePerRequestFilter::class]
        )
    ],
    excludeAutoConfiguration = [SecurityAutoConfiguration::class]
)
class MovieDetailsControllerIntegrationTest {
    @MockkBean
    private lateinit var movieDetailsFetcher: MovieDetailsFetcher

    @Autowired
    lateinit var mockMvc: MockMvc

    @Test
    fun `should return movie details`() {
        // given
        val movieId = UUID.randomUUID()
        val movieDetails = MovieDetails(
            name = "test-name",
            releaseDate = "test-release-date",
            description = "test-description",
            runtime = "test-runtime"
        )
        every { movieDetailsFetcher.getByMovieId(movieId) } answers { movieDetails }
        // when
        val result = mockMvc.get("/api/public/movie/$movieId/details")
        // then
        result
            .andExpect { status().isOk }
            .andExpect { jsonPath("$.name", `is`(movieDetails.name)) }
            .andExpect { jsonPath("$.releaseDate", `is`(movieDetails.releaseDate)) }
            .andExpect { jsonPath("$.description", `is`(movieDetails.description)) }
            .andExpect { jsonPath("$.runtime", `is`(movieDetails.runtime)) }
        verify(exactly = 1) { movieDetailsFetcher.getByMovieId(any()) }
    }

    @Test
    fun `should return not found`() {
        // given
        val movieId = UUID.randomUUID()
        every { movieDetailsFetcher.getByMovieId(movieId) } throws MovieNotFoundException(movieId)
        // when
        val result = mockMvc.get("/api/public/movie/$movieId/details")
        // then
        result.andExpect { status().isNotFound }
    }

    @Test
    fun `should return server error`() {
        // given
        val movieId = UUID.randomUUID()
        every { movieDetailsFetcher.getByMovieId(movieId) } throws MovieDetailsNotAvailableException(movieId)
        // when
        val result = mockMvc.get("/api/public/movie/$movieId/details")
        // then
        result.andExpect { status().isInternalServerError }
    }
}
