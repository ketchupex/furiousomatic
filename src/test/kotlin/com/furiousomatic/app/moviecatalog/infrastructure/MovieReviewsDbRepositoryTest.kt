package com.furiousomatic.app.moviecatalog.infrastructure

import io.mockk.mockk
import org.junit.Ignore
import org.junit.jupiter.api.Test

class MovieReviewsDbRepositoryTest {
    private val movieReviewsEntityRepository: MovieReviewsEntityRepository = mockk()
    private val dbMovieRepositoryTest = DbMovieReviewsRepository(movieReviewsEntityRepository)

    @Ignore
    @Test
    fun `should get movie reviews`() {
        // TODO
    }

    @Ignore
    @Test
    fun `should get empty reviews`() {
        // TODO
    }

    @Ignore
    @Test
    fun `should save movie reviews`() {
        // TODO
    }
}
