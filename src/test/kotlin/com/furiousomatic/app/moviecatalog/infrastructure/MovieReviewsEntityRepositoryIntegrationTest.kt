package com.furiousomatic.app.moviecatalog.infrastructure

import com.furiousomatic.app.showmanagement.tools.TransactioWrapperConfig
import com.furiousomatic.app.showmanagement.tools.TransactionWrapper
import com.furiousomatic.app.technical.JacksonConfig
import com.furiousomatic.app.technical.tools.DatabaseContainerInitializer
import com.furiousomatic.app.technical.tools.randomString
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.Import
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import java.util.UUID

@DataJpaTest(showSql = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(
    initializers = [DatabaseContainerInitializer::class]
)
@Import(TransactioWrapperConfig::class, JacksonConfig::class)
class MovieReviewsEntityRepositoryIntegrationTest {
    @Autowired
    private lateinit var reviewsRepository: MovieReviewsEntityRepository

    @Autowired
    private lateinit var movieRepository: MovieEntityRepository

    @Autowired
    private lateinit var transactionWrapper: TransactionWrapper

    @AfterEach
    fun cleanup() {
        reviewsRepository.deleteAll()
        movieRepository.deleteAll()
    }

    @Test
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun `should save and get reviews`() {
        // given
        val movie = MovieEntity(UUID.randomUUID(), randomString(), true)
        val movieReviews = MovieReviewsEntity(
            movie.id,
            ReviewsCol(
                mapOf(
                    UUID.randomUUID() to ReviewColData(5),
                    UUID.randomUUID() to ReviewColData(2),
                )
            )
        )
        transactionWrapper {
            movieRepository.save(movie)
        }
        // when
        transactionWrapper {
            reviewsRepository.save(movieReviews)
        }
        val result = reviewsRepository.findByMovieId(movie.id).get()
        // then
        expectThat(result).isEqualTo(movieReviews)
    }
}
