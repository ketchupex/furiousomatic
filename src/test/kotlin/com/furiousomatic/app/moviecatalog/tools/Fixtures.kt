package com.furiousomatic.app.moviecatalog.tools

import com.furiousomatic.app.moviecatalog.deepmodel.Movie
import com.furiousomatic.app.moviecatalog.deepmodel.MovieReviews
import com.furiousomatic.app.moviecatalog.deepmodel.Review
import com.furiousomatic.app.moviecatalog.deepmodel.Reviewer
import com.furiousomatic.app.technical.tools.randomInt
import com.furiousomatic.app.technical.tools.randomString
import java.util.UUID
import kotlin.random.Random.Default.nextBoolean

internal fun randomMovie(active: Boolean = nextBoolean()) = Movie(UUID.randomUUID(), randomString(), active)

internal fun randomMovieReviews(
    reviewByReviewerId: Map<UUID, Review> = mapOf(UUID.randomUUID() to Review(randomInt(5)))
) = MovieReviews(UUID.randomUUID(), reviewByReviewerId)

internal fun randomReviewer() = Reviewer(UUID.randomUUID())
