package com.furiousomatic.app.moviecatalog.deepmodel

import com.furiousomatic.app.moviecatalog.tools.randomMovieReviews
import com.furiousomatic.app.moviecatalog.tools.randomReviewer
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.isEqualTo
import strikt.assertions.isNull
import java.util.UUID

class MovieReviewsTest {

    @Test
    fun `should create snapshot`() {
        // given
        val movieId = UUID.randomUUID()
        val reviewByReviewerId = mapOf(UUID.randomUUID() to Review(4))
        val movie = MovieReviews(
            movieId = movieId,
            reviewByRevieverId = reviewByReviewerId
        )
        // when
        val snapshot = movie.toSnapshot()
        // then
        expectThat(snapshot.movieId).isEqualTo(movieId)
        expectThat(snapshot.reviewByReviewerId).isEqualTo(reviewByReviewerId)
    }

    @ParameterizedTest
    @ValueSource(ints = [1, 2, 3, 4, 5])
    fun `should add review`(rating: Int) {
        // given
        val reviews = randomMovieReviews()
        val reviewer = randomReviewer()
        // when
        val review = Review(rating)
        reviews.addReview(reviewer, review)
        val snapshot = reviews.toSnapshot()
        // then
        expectThat(snapshot.reviewByReviewerId[reviewer.id]).isEqualTo(review)
    }

    @Test
    fun `should not add review because it was already added`() {
        // given
        val oldReview = Review(3)
        val newReview = Review(4)
        val reviews = randomMovieReviews()
        val reviewer = randomReviewer()
        reviews.addReview(reviewer, oldReview)
        // then
        expectThrows<ReviewAlreadyExistsException> { reviews.addReview(reviewer, newReview) }
        val snapshot = reviews.toSnapshot()
        // then
        expectThat(snapshot.reviewByReviewerId[reviewer.id]).isEqualTo(oldReview)
    }

    @ParameterizedTest
    @ValueSource(ints = [-1, 0, 6, 7])
    fun `should not add review because of invalid review value`(rating: Int) {
        // given
        val review = Review(rating)
        val reviews = randomMovieReviews()
        val reviewer = randomReviewer()
        // then
        expectThrows<InvalidReviewRatingException> { reviews.addReview(reviewer, review) }
        val snapshot = reviews.toSnapshot()
        // then
        expectThat(snapshot.reviewByReviewerId[reviewer.id]).isNull()
    }

    @ParameterizedTest
    @ValueSource(ints = [1, 2, 3, 4, 5])
    fun `should change review`(newRating: Int) {
        // given
        val reviews = randomMovieReviews()
        val reviewer = randomReviewer()
        val oldReview = Review(4)
        val newReview = Review(newRating)
        reviews.addReview(reviewer, oldReview)
        // when
        reviews.changeReview(reviewer, newReview)
        val snapshot = reviews.toSnapshot()
        // then
        expectThat(snapshot.reviewByReviewerId[reviewer.id]).isEqualTo(newReview)
    }

    @Test
    fun `should not change review because it was not added before`() {
        // given
        val reviews = randomMovieReviews()
        val reviewer = randomReviewer()
        // when
        expectThrows<ReviewNotFoundException> { reviews.changeReview(reviewer, Review(4)) }
        val snapshot = reviews.toSnapshot()
        // then
        expectThat(snapshot.reviewByReviewerId[reviewer.id]).isNull()
    }

    @ParameterizedTest
    @ValueSource(ints = [-1, 0, 6, 7])
    fun `should not change review because of invalid review value`(newRating: Int) {
        // given
        val reviews = randomMovieReviews()
        val reviewer = randomReviewer()
        val oldReview = Review(4)
        val newReview = Review(newRating)
        reviews.addReview(reviewer, oldReview)
        // when
        expectThrows<InvalidReviewRatingException> { reviews.changeReview(reviewer, newReview) }
        val snapshot = reviews.toSnapshot()
        // then
        expectThat(snapshot.reviewByReviewerId[reviewer.id]).isEqualTo(oldReview)
    }
}
