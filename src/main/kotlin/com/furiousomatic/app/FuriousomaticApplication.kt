package com.furiousomatic.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class FuriousomaticApplication

fun main(args: Array<String>) {
    runApplication<FuriousomaticApplication>(*args)
}
