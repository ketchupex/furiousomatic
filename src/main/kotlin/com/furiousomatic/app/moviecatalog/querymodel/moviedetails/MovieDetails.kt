package com.furiousomatic.app.moviecatalog.querymodel.moviedetails

data class MovieDetails(
    val name: String?,
    val description: String?,
    val releaseDate: String?,
    val runtime: String?
)
