package com.furiousomatic.app.moviecatalog.querymodel.moviedetails

import java.util.UUID

interface MovieDetailsFetcher {
    fun getByMovieId(movieId: UUID): MovieDetails
}

class MovieDetailsNotAvailableException(movieId: UUID) :
    RuntimeException("Movie details are not available for id: $movieId")
