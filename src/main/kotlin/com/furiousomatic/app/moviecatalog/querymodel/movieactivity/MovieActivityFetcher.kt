package com.furiousomatic.app.moviecatalog.querymodel.movieactivity

import java.util.UUID

interface MovieActivityFetcher {
    fun findByMovieId(movieId: UUID): MovieActivity
}
