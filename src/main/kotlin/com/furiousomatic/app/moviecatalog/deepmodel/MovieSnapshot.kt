package com.furiousomatic.app.moviecatalog.deepmodel

import java.util.UUID

data class MovieSnapshot(
    val id: UUID,
    val imdbId: String,
    val active: Boolean
)
