package com.furiousomatic.app.moviecatalog.deepmodel

import java.util.UUID

internal interface MovieRepository {
    fun findById(id: UUID): Movie
    fun save(movie: Movie)
}
