package com.furiousomatic.app.moviecatalog.deepmodel

import java.util.UUID

internal interface MovieReviewsRepository {
    fun findByMovieId(movieId: UUID): MovieReviews
    fun save(movieReviews: MovieReviews)
}
