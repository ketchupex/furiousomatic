package com.furiousomatic.app.moviecatalog.deepmodel

import java.util.UUID

data class Reviewer(val id: UUID)
