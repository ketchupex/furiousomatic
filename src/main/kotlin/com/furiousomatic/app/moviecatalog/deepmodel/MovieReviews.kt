package com.furiousomatic.app.moviecatalog.deepmodel

import java.util.UUID

internal class MovieReviews(
    val movieId: UUID,
    reviewByRevieverId: Map<UUID, Review>
) {
    private val _reviewByReviewerId = reviewByRevieverId.toMutableMap()

    fun addReview(reviewer: Reviewer, review: Review) {
        val reviewerId = reviewer.id
        val previousRating = _reviewByReviewerId[reviewerId]
        validateReview(review)
        if (previousRating != null) {
            throw ReviewAlreadyExistsException(this.movieId, reviewerId)
        }
        _reviewByReviewerId[reviewerId] = review
    }

    fun changeReview(reviewer: Reviewer, review: Review) {
        val reviewerId = reviewer.id
        val previousRating = _reviewByReviewerId[reviewerId]
        validateReview(review)
        if (previousRating == null) {
            throw ReviewNotFoundException(this.movieId, reviewerId)
        }
        _reviewByReviewerId[reviewerId] = review
    }

    fun toSnapshot(): MovieReviewsSnapshot {
        return MovieReviewsSnapshot(movieId, _reviewByReviewerId.toMap())
    }

    private fun validateReview(review: Review) {
        val rating = review.rating
        if (rating < 1 || rating > 5) {
            throw InvalidReviewRatingException(review)
        }
    }
}
