package com.furiousomatic.app.moviecatalog.deepmodel

data class Review(
    val rating: Int
)
