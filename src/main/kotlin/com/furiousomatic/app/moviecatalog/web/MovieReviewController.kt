package com.furiousomatic.app.moviecatalog.web

import com.furiousomatic.app.moviecatalog.processmodel.addreview.AddReviewCommand
import com.furiousomatic.app.moviecatalog.processmodel.changereview.ChangeReviewCommand
import com.furiousomatic.app.technical.ApiConstants
import com.furiousomatic.app.technical.CommandHandler
import io.swagger.annotations.ApiModelProperty
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("${ApiConstants.SECURED}/movie/{movieId}/review")
class MovieReviewController(
    private val addReviewCommandHandler: CommandHandler<AddReviewCommand, *>,
    private val changeReviewCommandHandler: CommandHandler<ChangeReviewCommand, *>
) : MovieReviewApiDocs {

    @PostMapping
    override fun addReview(
        @PathVariable("movieId") movieId: UUID,
        @RequestBody request: ReviewRequest
    ) {
        addReviewCommandHandler.handle(
            AddReviewCommand(
                movieId = movieId,
                rating = request.rating
            )
        )
    }

    @PutMapping
    override fun changeReview(
        @PathVariable("movieId") movieId: UUID,
        @RequestBody request: ReviewRequest
    ) {
        changeReviewCommandHandler.handle(
            ChangeReviewCommand(
                movieId = movieId,
                rating = request.rating
            )
        )
    }
}

data class ReviewRequest(
    @ApiModelProperty(
        notes = "Must be between 1 and 5"
    )
    val rating: Int
)
