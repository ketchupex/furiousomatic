package com.furiousomatic.app.moviecatalog.web

import com.furiousomatic.app.moviecatalog.querymodel.moviedetails.MovieDetails
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import java.util.UUID

@Api(description = "Movie Details", tags = ["Movie Details"])
interface MovieDetailsApiDocs {
    @ApiOperation("Get movie details")
    fun getMovieDetails(
        @ApiParam(
            value = "Movie Id",
            example = "68c5970c-92cf-4083-9008-207a691c6ab9",
            required = true
        ) movieId: UUID
    ): MovieDetails
}
