package com.furiousomatic.app.moviecatalog.processmodel.changereview

import com.furiousomatic.app.technical.Command
import java.util.UUID

data class ChangeReviewCommand(
    val movieId: UUID,
    val rating: Int
) : Command
