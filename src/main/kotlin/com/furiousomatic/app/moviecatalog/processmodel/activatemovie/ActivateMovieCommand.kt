package com.furiousomatic.app.moviecatalog.processmodel.activatemovie

import com.furiousomatic.app.technical.Command
import java.util.UUID

data class ActivateMovieCommand(val movieId: UUID) : Command
