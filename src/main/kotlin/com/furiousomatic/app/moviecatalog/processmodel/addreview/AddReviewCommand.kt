package com.furiousomatic.app.moviecatalog.processmodel.addreview

import com.furiousomatic.app.technical.Command
import java.util.UUID

data class AddReviewCommand(
    val movieId: UUID,
    val rating: Int
) : Command
