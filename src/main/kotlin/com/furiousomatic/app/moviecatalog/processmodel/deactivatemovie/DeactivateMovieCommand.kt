package com.furiousomatic.app.moviecatalog.processmodel.deactivatemovie

import com.furiousomatic.app.technical.Command
import java.util.UUID

data class DeactivateMovieCommand(val movieId: UUID) : Command
