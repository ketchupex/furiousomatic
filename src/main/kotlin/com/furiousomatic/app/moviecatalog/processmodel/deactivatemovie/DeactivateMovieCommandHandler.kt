package com.furiousomatic.app.moviecatalog.processmodel.deactivatemovie

import com.furiousomatic.app.moviecatalog.deepmodel.MovieRepository
import com.furiousomatic.app.technical.CommandHandler
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
internal class DeactivateMovieCommandHandler(private val movieRepository: MovieRepository) :
    CommandHandler<DeactivateMovieCommand, Unit> {
    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    override fun handle(command: DeactivateMovieCommand) {
        val movie = movieRepository.findById(command.movieId)
        movie.deactivate()
        movieRepository.save(movie)
    }
}
