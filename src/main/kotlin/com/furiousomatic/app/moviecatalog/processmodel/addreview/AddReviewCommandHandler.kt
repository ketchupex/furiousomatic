package com.furiousomatic.app.moviecatalog.processmodel.addreview

import com.furiousomatic.app.moviecatalog.deepmodel.MovieReviewsRepository
import com.furiousomatic.app.moviecatalog.deepmodel.Review
import com.furiousomatic.app.moviecatalog.deepmodel.Reviewer
import com.furiousomatic.app.technical.CommandHandler
import com.furiousomatic.app.usermanagement.user.CurrentUserService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
internal class AddReviewCommandHandler(
    private val movieReviewsRepository: MovieReviewsRepository,
    private val currentUserService: CurrentUserService
) : CommandHandler<AddReviewCommand, Unit> {

    @PreAuthorize("hasRole('CUSTOMER')")
    @Transactional
    override fun handle(command: AddReviewCommand) {
        val movie = movieReviewsRepository.findByMovieId(command.movieId)
        movie.addReview(Reviewer(currentUserService.getCurrentUserId()), Review(command.rating))
        movieReviewsRepository.save(movie)
    }
}
