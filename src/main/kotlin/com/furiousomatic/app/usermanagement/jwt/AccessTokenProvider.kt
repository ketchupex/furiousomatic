package com.furiousomatic.app.usermanagement.jwt

import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.JWSSigner
import com.nimbusds.jose.KeyException
import com.nimbusds.jose.crypto.MACSigner
import com.nimbusds.jose.crypto.MACVerifier
import com.nimbusds.jwt.JWTClaimsSet
import com.nimbusds.jwt.SignedJWT
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component
import java.time.Duration
import java.util.Date

interface AccessTokenProvider {
    fun getAccessToken(userDetails: UserDetails): String
    fun extractAuthentication(token: String): Authentication
}

@Component
internal class AccessTokenProviderImpl(
    @Value("\${furiousomatic.jwt.secret}")
    private val secret: String,
    private val userDetailsService: JwtUserDetailsService
) : AccessTokenProvider {
    companion object {
        private val JWT_TOKEN_VALIDITY = Duration.ofHours(5)
    }

    private val verifier = MACVerifier(secret)

    override fun getAccessToken(userDetails: UserDetails): String {
        return generateToken(emptyMap(), userDetails.username)
    }

    override fun extractAuthentication(token: String): Authentication {
        val claims = validateAndGetClaims(token)
        if (isTokenExpired(claims)) {
            throw KeyException("Expired key")
        }
        val username = claims.subject!!
        val userDetails = userDetailsService.loadUserByUsername(username)
        if (username != userDetails.username) {
            throw KeyException("Username not matching")
        }
        return UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
    }

    // while creating the token -
    // 1. Define  claims of the token, like Issuer, Expiration, Subject, and the ID
    // 2. Sign the JWT using the HS512 algorithm and secret key.
    // 3. According to JWS Compact Serialization(https://tools.ietf.org/html/draft-ietf-jose-json-web-signature-41#section-3.1)
    //   compaction of the JWT to a URL-safe string
    private fun generateToken(claimsMap: Map<String, Any>, subject: String): String {
        val claimsBuilder = JWTClaimsSet.Builder()
            .issueTime(Date(System.currentTimeMillis()))
            .expirationTime(Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY.toMillis()))
            .subject(subject)
        claimsMap.forEach { (key, value) -> claimsBuilder.claim(key, value) }
        val signedJWT = SignedJWT(JWSHeader(JWSAlgorithm.HS512), claimsBuilder.build())
        val signer: JWSSigner = MACSigner(secret)
        signedJWT.sign(signer)
        return signedJWT.serialize()
    }

    fun validateAndGetClaims(token: String): JWTClaimsSet {
        return SignedJWT.parse(token)!!.let {
            it.verify(verifier)
            it.jwtClaimsSet
        }
    }

    // check if the token has expired
    private fun isTokenExpired(jwtClaimsSet: JWTClaimsSet): Boolean {
        return jwtClaimsSet.expirationTime!!.before(Date())
    }
}
