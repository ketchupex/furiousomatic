package com.furiousomatic.app.usermanagement.jwt

import com.furiousomatic.app.usermanagement.user.AppUserDetails
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.util.UUID

@Service
internal class JwtUserDetailsService(private val passwordEncoder: PasswordEncoder) : UserDetailsService {
    val users = listOf(
        AppUserDetails(
            id = UUID.fromString("a87279b4-9707-47ab-95e3-269f1a1154d7"),
            username = "CUSTOMER",
            password = passwordEncoder.encode("CUSTOMER"),
            authorities = listOf(SimpleGrantedAuthority("ROLE_CUSTOMER"))
        ),
        AppUserDetails(
            id = UUID.fromString("488d3130-3f1f-4567-8f94-19a8a4c6e51b"),
            username = "ADMIN",
            password = passwordEncoder.encode("ADMIN"),
            authorities = listOf(SimpleGrantedAuthority("ROLE_ADMIN"))
        )
    ).associateBy { it.username }

    override fun loadUserByUsername(username: String?): UserDetails {
        return users[username] ?: throw UsernameNotFoundException("Username not found: $username")
    }
}
