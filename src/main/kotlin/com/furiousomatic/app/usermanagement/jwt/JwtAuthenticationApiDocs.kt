package com.furiousomatic.app.usermanagement.jwt

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.http.ResponseEntity

@Api(description = "Authentication", tags = ["Authentication"])
internal interface JwtAuthenticationApiDocs {
    @ApiOperation("Get jwt token")
    fun createAuthenticationToken(authenticationRequest: JwtRequest): ResponseEntity<JwtTokenResponse>
}
