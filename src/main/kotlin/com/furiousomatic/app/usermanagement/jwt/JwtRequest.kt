package com.furiousomatic.app.usermanagement.jwt

internal data class JwtRequest(val username: String, val password: String)
