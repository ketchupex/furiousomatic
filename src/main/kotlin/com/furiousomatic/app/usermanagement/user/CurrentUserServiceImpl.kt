package com.furiousomatic.app.usermanagement.user

import org.springframework.security.access.AccessDeniedException
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import java.util.UUID

interface CurrentUserService {
    fun getCurrentUserId(): UUID
}

@Component
internal class CurrentUserServiceImpl : CurrentUserService {
    override fun getCurrentUserId(): UUID {
        val authentication = getAuthentication()
        val principal = authentication?.principal
        val authenticated = authentication?.isAuthenticated ?: false
        return if (authenticated && principal != null && principal is AppUserDetails) {
            principal.getId()
        } else {
            throw AccessDeniedException("User not logged")
        }
    }

    private fun getAuthentication(): Authentication? {
        return SecurityContextHolder.getContext().authentication
    }
}
