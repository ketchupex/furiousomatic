package com.furiousomatic.app.showmanagement.deepmodel

import com.furiousomatic.app.showmanagement.shared.FULL_DAY
import java.time.Clock
import java.time.Duration
import java.time.Instant
import java.util.UUID
import javax.money.MonetaryAmount

internal class ShowTime(
    val id: UUID,
    val screeningRoomId: UUID,
    movie: Movie,
    start: Instant,
    end: Instant,
    price: MonetaryAmount,
    val clock: Clock
) {
    var movie: Movie = movie
        private set
    var start: Instant = start
        private set
    var end: Instant = end
        private set
    var price: MonetaryAmount = price
        private set

    fun overlaps(showTime: ShowTime): Boolean {
        return this.start < showTime.end && this.end > showTime.start
    }

    fun toSnapshot(): ShowTimeSnapshot {
        return ShowTimeSnapshot(id, screeningRoomId, movie, start, end, price)
    }

    fun changePrice(price: MonetaryAmount) {
        val durationToShow = Duration.between(clock.instant(), start)
        if (durationToShow < FULL_DAY) {
            throw TooLateOperationException(FULL_DAY)
        } else {
            this.price = price
        }
    }
}
