package com.furiousomatic.app.showmanagement.deepmodel

import java.time.Duration
import java.time.Instant
import java.util.UUID

sealed class ShowManagementException(message: String?, cause: Throwable? = null) : RuntimeException(message, cause)

class ActiveMovieRequiredException(movieId: UUID) :
    ShowManagementException("Movie id: $movieId must be active for this operation")

class TooLateOperationException(minDuration: Duration) :
    ShowManagementException("It is too late for this operation. Min hours to operation: ${minDuration.toHours()}")

class ShowTimeNotFoundException(showTimeId: UUID) :
    ShowManagementException("Show time not found for id: $showTimeId")

class InvalidShowtimeDateException(start: Instant, end: Instant) :
    ShowManagementException("Invalid showtime date. Start: ${start.epochSecond}, end: ${end.epochSecond}")

class ShowTimeOverlapsException(screeningRoomId: UUID, start: Instant, end: Instant) :
    ShowManagementException("New showtime overlaps another show for room: $screeningRoomId. Start: ${start.epochSecond}, end: ${end.epochSecond}")

class ScreeningRoomNotFoundException(screeningRoomId: UUID) :
    ShowManagementException("Could not find screening room for id: $screeningRoomId")
