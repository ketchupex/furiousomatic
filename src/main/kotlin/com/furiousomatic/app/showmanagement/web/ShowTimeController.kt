package com.furiousomatic.app.showmanagement.web

import com.furiousomatic.app.showmanagement.querymodel.ShowTimeDto
import com.furiousomatic.app.showmanagement.querymodel.ShowTimesFetcher
import com.furiousomatic.app.technical.ApiConstants
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("${ApiConstants.PUBLIC}/show-time")
class ShowTimeController(private val showTimesFetcher: ShowTimesFetcher) : ShowTimeApiDocs {

    @GetMapping
    override fun getShowTimes(@RequestParam("movieId", required = true) movieId: UUID): GetShowTimesResponse {
        val showTimes = showTimesFetcher.getForMovie(movieId)
        return GetShowTimesResponse(showTimes)
    }
}

data class GetShowTimesResponse(val showTimes: List<ShowTimeDto>)
