package com.furiousomatic.app.showmanagement.web

import com.furiousomatic.app.showmanagement.processmodel.addshowtime.AddShowTimeCommand
import com.furiousomatic.app.showmanagement.processmodel.changeprice.ChangeShowPriceCommand
import com.furiousomatic.app.showmanagement.processmodel.deleteshowtime.DeleteShowTimeCommand
import com.furiousomatic.app.technical.ApiConstants
import com.furiousomatic.app.technical.CommandHandler
import io.swagger.annotations.ApiModelProperty
import org.javamoney.moneta.Money
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.time.Instant
import java.util.UUID

@RestController
@RequestMapping("${ApiConstants.SECURED}/management/screening-room/{screeningRoomId}/show-time")
class ShowTimeManagementController(
    private val addShowTimeCommandHandler: CommandHandler<AddShowTimeCommand, *>,
    private val changeShowPriceCommandHandler: CommandHandler<ChangeShowPriceCommand, *>,
    private val deleteShowTimeCommandHandler: CommandHandler<DeleteShowTimeCommand, *>,
) : ShowTimeManagementApiDocs {

    @PostMapping
    override fun addShowTime(
        @PathVariable("screeningRoomId") screeningRoomId: UUID,
        @RequestBody request: AddShowTimeRequest
    ) {
        val command = AddShowTimeCommand(
            screeningRoomId,
            request.movieId,
            request.start,
            request.end,
            Money.of(request.price, request.currency)
        )
        addShowTimeCommandHandler.handle(command)
    }

    @PutMapping("/{showTimeId}")
    override fun changeShowTimePrice(
        @PathVariable("screeningRoomId") screeningRoomId: UUID,
        @PathVariable("showTimeId") showTimeId: UUID,
        @RequestBody request: ChangeShowPriceRequest
    ) {
        val command = ChangeShowPriceCommand(
            showTimeId,
            Money.of(request.price, request.currency)
        )
        changeShowPriceCommandHandler.handle(command)
    }

    @DeleteMapping("/{showTimeId}")
    override fun deleteShowTime(
        @PathVariable("screeningRoomId") screeningRoomId: UUID,
        @PathVariable("showTimeId") showTimeId: UUID
    ) {
        val command = DeleteShowTimeCommand(screeningRoomId, showTimeId)
        deleteShowTimeCommandHandler.handle(command)
    }
}

data class AddShowTimeRequest(
    @ApiModelProperty(
        example = "68c5970c-92cf-4083-9008-207a691c6ab9"
    )
    val movieId: UUID,
    val start: Instant,
    val end: Instant,
    @ApiModelProperty(
        example = "10"
    )
    val price: BigDecimal,
    @ApiModelProperty(
        example = "USD"
    )
    val currency: String
)

data class ChangeShowPriceRequest(
    @ApiModelProperty(
        example = "10"
    )
    val price: BigDecimal,
    @ApiModelProperty(
        example = "USD"
    )
    val currency: String
)
