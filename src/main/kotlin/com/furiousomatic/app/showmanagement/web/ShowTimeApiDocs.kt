package com.furiousomatic.app.showmanagement.web

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import java.util.UUID

@Api(description = "Show Times", tags = ["Show Times"])
interface ShowTimeApiDocs {
    @ApiOperation("Get show times for movie")
    fun getShowTimes(
        @ApiParam(
            value = "Movie Id",
            example = "68c5970c-92cf-4083-9008-207a691c6ab9",
            required = true
        ) movieId: UUID
    ): GetShowTimesResponse
}
