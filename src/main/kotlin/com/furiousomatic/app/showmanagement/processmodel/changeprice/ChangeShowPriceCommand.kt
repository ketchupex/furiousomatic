package com.furiousomatic.app.showmanagement.processmodel.changeprice

import com.furiousomatic.app.technical.Command
import java.util.UUID
import javax.money.MonetaryAmount

data class ChangeShowPriceCommand(
    val showTimeId: UUID,
    val price: MonetaryAmount,
) : Command
