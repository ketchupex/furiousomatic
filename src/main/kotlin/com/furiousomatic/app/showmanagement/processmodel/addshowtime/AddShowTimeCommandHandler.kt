package com.furiousomatic.app.showmanagement.processmodel.addshowtime

import com.furiousomatic.app.showmanagement.deepmodel.ActiveMovieRequiredException
import com.furiousomatic.app.showmanagement.deepmodel.Movie
import com.furiousomatic.app.showmanagement.deepmodel.MovieActivityChecker
import com.furiousomatic.app.showmanagement.deepmodel.ScreeningRoomRepository
import com.furiousomatic.app.technical.CommandHandler
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
internal class AddShowTimeCommandHandler(
    private val movieActivityChecker: MovieActivityChecker,
    private val screeningRoomRepository: ScreeningRoomRepository
) : CommandHandler<AddShowTimeCommand, Unit> {

    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    override fun handle(command: AddShowTimeCommand) {
        val movieId = command.movieId
        val isMovieActive = movieActivityChecker.isActive(movieId)
        if (isMovieActive) {
            val movie = Movie(movieId)
            val screeningRoom = screeningRoomRepository.findById(command.screeningRoomId)
            screeningRoom.addShowTime(movie, command.start, command.end, command.price)
            screeningRoomRepository.save(screeningRoom)
        } else {
            throw ActiveMovieRequiredException(movieId)
        }
    }
}
