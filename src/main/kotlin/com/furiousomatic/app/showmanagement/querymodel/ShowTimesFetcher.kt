package com.furiousomatic.app.showmanagement.querymodel

import java.math.BigDecimal
import java.time.Instant
import java.util.UUID

interface ShowTimesFetcher {
    fun getForMovie(movieId: UUID): List<ShowTimeDto>
}

data class ShowTimeDto(
    val id: UUID,
    val start: Instant,
    val end: Instant,
    val price: BigDecimal,
    val currency: String
)
