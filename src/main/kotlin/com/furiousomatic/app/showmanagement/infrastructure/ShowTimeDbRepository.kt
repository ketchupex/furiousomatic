package com.furiousomatic.app.showmanagement.infrastructure

import com.furiousomatic.app.showmanagement.deepmodel.Movie
import com.furiousomatic.app.showmanagement.deepmodel.ShowTime
import com.furiousomatic.app.showmanagement.deepmodel.ShowTimeNotFoundException
import com.furiousomatic.app.showmanagement.deepmodel.ShowTimeRepository
import com.furiousomatic.app.showmanagement.deepmodel.ShowTimeSnapshot
import org.javamoney.moneta.Money
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.math.BigDecimal
import java.time.Clock
import java.time.Instant
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Repository
internal class ShowTimeDbRepository(
    private val showTimeEntityRepository: ShowTimeEntityRepository,
    private val clock: Clock
) :
    ShowTimeRepository {
    override fun findById(id: UUID): ShowTime {
        return showTimeEntityRepository.findById(id)
            .map { it.toDomain(clock) }
            .orElseThrow { ShowTimeNotFoundException(id) }
    }

    override fun save(showTime: ShowTime) {
        val entity = ShowTimeEntity.fromDomain(showTime)
        showTimeEntityRepository.save(entity)
    }
}

@Table(name = "SM_SHOW_TIME")
@Entity
internal data class ShowTimeEntity(
    @Id
    @Column(name = "ID")
    val id: UUID,
    @Column(name = "SCREENING_ROOM_ID")
    val screeningRoomId: UUID,
    @Column(name = "MOVIE_ID")
    val movieId: UUID,
    @Column(name = "START_DATE")
    val start: Instant,
    @Column(name = "END_DATE")
    val end: Instant,
    @Column(name = "PRICE")
    val price: BigDecimal,
    @Column(name = "CURRENCY")
    val currency: String
) {

    companion object {
        fun fromDomain(showTime: ShowTime): ShowTimeEntity {
            return ShowTimeEntity(
                id = showTime.id,
                screeningRoomId = showTime.screeningRoomId,
                movieId = showTime.movie.id,
                start = showTime.start,
                end = showTime.end,
                price = showTime.price.number.numberValue(BigDecimal::class.java),
                currency = showTime.price.currency.currencyCode
            )
        }

        fun fromSnapshot(showTime: ShowTimeSnapshot): ShowTimeEntity {
            return ShowTimeEntity(
                id = showTime.id,
                screeningRoomId = showTime.screeningRoomId,
                movieId = showTime.movie.id,
                start = showTime.start,
                end = showTime.end,
                price = showTime.getPriceAmount(),
                currency = showTime.getCurrency()
            )
        }
    }

    fun toDomain(clock: Clock): ShowTime {
        return ShowTime(
            id = id,
            screeningRoomId = screeningRoomId,
            movie = Movie(movieId),
            start = start,
            end = end,
            price = Money.of(price, currency),
            clock = clock
        )
    }
}

@Repository
internal interface ShowTimeEntityRepository : CrudRepository<ShowTimeEntity, UUID> {
    fun findByMovieId(movieId: UUID): List<ShowTimeEntity>
}
