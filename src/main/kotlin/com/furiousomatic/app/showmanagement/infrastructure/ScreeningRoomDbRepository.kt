package com.furiousomatic.app.showmanagement.infrastructure

import com.furiousomatic.app.showmanagement.deepmodel.ScreeningRoom
import com.furiousomatic.app.showmanagement.deepmodel.ScreeningRoomRepository
import com.furiousomatic.app.showmanagement.deepmodel.ShowTimeNotFoundException
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.time.Clock
import java.util.UUID
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.Table

@Repository
internal class ScreeningRoomDbRepository(
    private val screeningRoomEntityRepository: ScreeningRoomEntityRepository,
    private val clock: Clock
) :
    ScreeningRoomRepository {
    override fun findById(id: UUID): ScreeningRoom {
        return screeningRoomEntityRepository.findById(id)
            .map {
                ScreeningRoom(
                    id = it.id,
                    clock = clock,
                    showTimes = it.showTimes.map { showTime -> showTime.toDomain(clock) }
                )
            }
            .orElseThrow { ShowTimeNotFoundException(id) }
    }

    override fun save(screeningRoom: ScreeningRoom) {
        val entity = ScreeningRoomEntity(
            id = screeningRoom.id,
            showTimes = screeningRoom.getShowTimes().map {
                ShowTimeEntity.fromSnapshot(it)
            }
        )
        screeningRoomEntityRepository.save(entity)
    }
}

@Table(name = "SM_SCREENING_ROOM")
@Entity
internal data class ScreeningRoomEntity(
    @Id
    @Column(name = "ID")
    val id: UUID,
    @OneToMany(
        cascade = [CascadeType.ALL],
        orphanRemoval = true,
        fetch = FetchType.EAGER
    )
    @JoinColumn(name = "SCREENING_ROOM_ID")
    val showTimes: List<ShowTimeEntity>
)

@Repository
internal interface ScreeningRoomEntityRepository : CrudRepository<ScreeningRoomEntity, UUID>
