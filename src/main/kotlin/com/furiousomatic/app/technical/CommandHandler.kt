package com.furiousomatic.app.technical

interface CommandHandler<in T : Command, out U> {
    fun handle(command: T): U
}
