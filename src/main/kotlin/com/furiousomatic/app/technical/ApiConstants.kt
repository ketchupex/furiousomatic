package com.furiousomatic.app.technical

class ApiConstants {
    companion object {
        const val PUBLIC = "/api/public"
        const val SECURED = "/api/secured"
    }
}
